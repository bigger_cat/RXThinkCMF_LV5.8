<?php


namespace App\Models;

/**
 * 权限管理-模型
 * @author 牧羊人
 * @since 2020/8/29
 * Class AdminRomModel
 * @package App\Models
 */
class AdminRomModel extends BaseModel
{
    // 设置数据表
    protected $table = 'admin_rom';

    /**
     * 获取权限列表
     * @param $adminId 人员ID
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * @since: 2020/6/30
     * @author 牧羊人
     */
    public function getPermissionList($adminId)
    {

        $adminModel = new AdminModel();
        $adminInfo = $adminModel->find($adminId);
        $adminInfo = !empty($adminInfo) ? $adminInfo->toArray() : $adminInfo;
        $menuList = $this->getPermissionMenu($adminInfo['role_ids'], $adminId, 1, 0);
        return $menuList;
    }

    /**
     * 获取权限菜单
     * @param $roleIds
     * @param $adminId
     * @param $type
     * @param $pid
     * @return mixed
     * @since 2020/8/19
     * @author 牧羊人
     */
    public function getPermissionMenu($roleIds, $adminId, $type, $pid)
    {
        $map1 = [];
        if ($roleIds) {
            $map1 = [
                ['r.type', '=', 1],
                ['r.type_id', 'in', $roleIds],
            ];
        }
        $map2 = [
            ['r.type', '=', 2],
            ['r.type_id', '=', $adminId],
        ];
        $map = [$map1, $map2];
        $menuModel = new MenuModel();

        $menuList = $menuModel::from("menu as m")
            ->select('m.*')
            ->join('admin_rom as r', 'r.menu_id', '=', 'm.id')
            ->distinct(true)
            ->where(function ($query) use ($map) {
                $query->whereOr($map);
            })
            ->where('m.type', '=', $type)
            ->where('m.pid', '=', $pid)
            ->where('m.status', '=', 1)
            ->where('m.mark', '=', 1)
            ->orderBy('m.pid')
            ->orderBy('m.sort')
            ->get()->toArray();
        if (!empty($menuList)) {
            $type += 1;
            if ($type <= 4) {
                foreach ($menuList as &$val) {
                    $childList = $this->getPermissionMenu($roleIds, $adminId, $type, $val['id']);
                    if (is_array($childList) && !empty($childList)) {
                        $val['children'] = $childList;
                    }
                }
            }
        }
        return $menuList;
    }
}
